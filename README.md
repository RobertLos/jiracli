## Jira command line interface

This terminal based python application allows to interact with Atlassian Jira from the command line.
It is designed to interact with Jira through terminal commands only.
The first setup is only a demo.

## Background documentation
The application is written in Python 2.7 using the jira library as described in https://pythonhosted.org/jira/


### Installation
The program requires some libraries to be present for python. Best to work in a virtual env, but 
dependencies can also be installed on the standard python installation off the host system.

### Requirements
The minimum requirements to be fulfilled are:

 * python 2.7
 * pip 8.1.2

Load the requirements file using the following command:
```
$ pip install -U pip
$ pip install -r config/requirements.txt
```

## Example
The program can be started from the installation folder by:

```
$ pyjiratest.py --host=https://rotterdamsche-jongens.atlassian.net --user=robert@robertlos.nl --password=PASSWORD --show_issues CD FST
```

Enter the correct Password on the command line. A future version might include reading credentials from a settings file or asking interactively

## Getting help
Help can be obtained by using the --help flag

```
$ python  ./pyjiratest.py --help
Usage: pyjiratest.py [options] project1 [project2 ...]

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -H SERVER, --host=SERVER
                        URL of the server
  -u USER, --user=USER  User name
  -i, --show_issues     Show task list
  -o, --show_open_only  Show open issues only
  -v, --show_versions   Show version list of the current project
  -s, --show_subtasks   Show subtasks of issues (usually stories)
  -p PASSWORD, --password=PASSWORD
                        Password van de gebruiker
```