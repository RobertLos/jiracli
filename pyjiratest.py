#!/usr/bin/python

from optparse import OptionParser
from jira import JIRA
import logging
import pyttsx as tts

__author__ = 'Robert Los'

"""
script to print an overview of jira projects and get further information through the command line.
The program is meant as an example of the jira module for python

Runs with python 2.7+

usage: pyjiratest -H host -u user -p password [[job] ...] [--help]

Reference for the jira interface in https://media.readthedocs.org/pdf/jira/stable/jira.pdf

For now just dumping the stack.
author: Robert Los
date: 19-06-2015
revision history:
    19-06-2015: Initial setup - simple dump of stack of a specific Jira Issues
    04-08-2015: Extended with also show versions and more details on issues
    11-10-2016: Adapted for EBPI
"""

DEFAULT_USER = "anonymous"
DEFAULT_PASSWORD = ""
DEFAULT_SERVER = "https://rotterdamschejongens.atlassian.com"

LOG_FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.WARNING, lformat=LOG_FORMAT)


class JiraInstance:
    def __init__(self, url, user, password, speach=False):
        self.INSTANCE = self.connect(url, user, password)
        self.speach = speach

        if self.speach:
            self.engine = tts.init()
        self.start_speach()

    def start_speach(self):
        if self.speach:
            self.engine = tts.init()
            self.engine.setProperty('rate', 150)
            self.engine.setProperty('voice', 11)

    @staticmethod
    def connect(url, user, password):
        """
        Verbind met de Jira instance
        :param url: string URL of the Jira instance
        :param user: stringUsername (must be there)
        :param password: string Provided password. When empty ask for one on the command line
        :return: handler Jira connection
        """
        try:
            server = {
                'server': url
            }

            jira_connection = JIRA(server, basic_auth=(user, password))
            return jira_connection
        except Exception, e:
            logging.error("Failed to connect to JIRA: %s" % e)
            exit(1)

    def print_project(self, project, options):
        """
        Genereer alle gegevens van een jira project
        """
        if self.speach:
            self.engine.say(u'Attention. We are now getting al tasks in project %s' % self.INSTANCE.project(project).name)
            self.engine.runAndWait()
        print("________________________________________________________________________")
        print("Overview of project '%s' (%s)" %
              (self.INSTANCE.project(project).name, project))
        if options.show_issues:
            self.print_issues(project,
                              options.show_subtasks,
                              options.show_open_only,
                              not options.all_sprints,
                              options.show_for_all_users)
        if options.show_versions:
            self.print_versions(project)

    def print_issues(self, project, print_subtasks=False, show_open_only=False,
                     show_current_only=True, show_all_users=False):
        """
        print een overzicht van de openstaande issues in een project
        """
        print("")
        search_string = 'project=%s' % project
        if not show_all_users:
            sentence = 'Issues in project %s for user %s' % \
                        (project, self.INSTANCE.current_user())
            print sentence
            self.say(sentence)
            search_string += ' AND assignee=currentuser()'
        else:
            sentence = 'Issues in project %s for all users' % project
            print sentence
            self.say(sentence)

        print('===============')
        count = 0
        nr_done = 0
        if show_current_only:
            search_string += ' AND sprint IN openSprints()'

        jira_issue_list = self.INSTANCE.search_issues(search_string, maxResults=999)
        for jira_issue in jira_issue_list:
            if jira_issue.fields.status.name == 'Done':
                if not show_open_only:
                    sentence = "%s %s - %s: is closed on %s" % \
                          (jira_issue.fields.issuetype, jira_issue.key, jira_issue.fields.summary,
                           jira_issue.fields.resolutiondate)
                    print sentence
                    self.say(sentence)
                nr_done += 1
            else:
                sentence = "%s %s - %s: has status %s" % \
                      (jira_issue.fields.issuetype, jira_issue.key, jira_issue.fields.summary,
                       jira_issue.fields.status)
                print sentence
                self.say(sentence)
            if print_subtasks:
                for sub_issue in jira_issue.fields.subtasks:
                    print("    subtask: %s " % sub_issue.key)

            count += 1
        print("(%s of %s issues are done)" % (nr_done, count))

    def say(self, sentence):
        self.start_speach()
        self.engine.say(sentence)
        self.run_voice()

    def print_versions(self, project):
        """
        print een lijst van alle beheerde versions in het project
        """
        print("")
        print('Versions in %s' % project)
        print('================')
        count = 0
        jira_version_list = self.INSTANCE.project_versions(project)
        for version in jira_version_list:
            count += 1
            print(version.name)
        print('(%s versions found)' % count)

    def run_voice(self):
        if self.speach:
            self.engine.runAndWait()


def parse_commandline():
    """
    Read the command line parameters.

    :return: command line option dictionary and argument list
    """
    parser = OptionParser(usage="%prog [options] project1 [project2 ...]", version="%prog 1.0")
    parser.add_option('-H', '--host', action="store", dest="server", default=DEFAULT_SERVER,
                      help='URL of the server')
    parser.add_option('-u', '--user', action="store", dest="user", default=DEFAULT_USER,
                      help='User name')
    parser.add_option('-i', '--show_issues', action="store_true", dest="show_issues",
                      default=False, help='Show task list')
    parser.add_option('-o', '--show_open_only', action="store_true", dest="show_open_only",
                      default=False, help='Show open issues only')
    parser.add_option('-v', '--show_versions', action="store_true", dest="show_versions",
                      default=False, help='Show version list of the current project')
    parser.add_option('-s', '--show_subtasks', action="store_true", dest="show_subtasks",
                      default=False, help='Show subtasks of issues (usually stories)'),
    parser.add_option('-n', '--narate', action="store_true", dest="speach",
                      default=False, help='Use speech to speak audible'),
    parser.add_option('-p', '--password', action="store", dest="password",
                      default=DEFAULT_PASSWORD, help='Password van de gebruiker')
    parser.add_option('-a', '--all_sprints', action="store_true", dest="all_sprints",
                      default=False, help='Show results for all sprints (not just the current)')
    parser.add_option('-A', '--all_users', action="store_true", dest="show_for_all_users",
                      default=False, help='Show items for all users')

    (options, args) = parser.parse_args()

    if len(args) < 1:
        logging.error("Argumenten vergeten")
        exit(1)

    return options, args


def main():
    """
    This is where it all happens
    :return: void
    """
    logging.info("Jira information monitor")
    (options, args) = parse_commandline()

    jira = JiraInstance(options.server, options.user, options.password, speach=options.speach)
    for project in args:
        jira.print_project(project, options)
    jira.run_voice()

if __name__ == "__main__":
    main()
